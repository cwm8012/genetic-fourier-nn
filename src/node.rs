use std::f64::consts::PI;

const MAX: f64 = 1.0;
const MIN: f64 = -1.0;

#[derive(Clone)]
pub struct Node {
    num_inputs: usize,
    fourier_n: usize,
    input_weights: Vec<f64>,
    cos: Vec<f64>,
    sin: Vec<f64>,
    fouier_bias: f64,
}

pub fn new(num_inputs: usize, fourier_n: usize) -> Node {
    let mut node: Node = Node {
        num_inputs,
        fourier_n,
        input_weights: Vec::with_capacity(num_inputs),
        cos: Vec::with_capacity(fourier_n),
        sin: Vec::with_capacity(fourier_n),
        fouier_bias: 0.0,
    };

    for _ in 0..num_inputs {
        node.input_weights.push(rand::random::<f64>() * 2.0 - 1.0);
    }

    for _ in 0..fourier_n {
        node.cos.push(rand::random::<f64>() * 2.0 - 1.0);
        node.sin.push(rand::random::<f64>() * 2.0 - 1.0);
    }

    node
}

pub fn mutate(node: &Node, lr: f64) -> Node {
    let mut new_node: Node = node.clone();

    for i in 0..new_node.num_inputs {
        let new_weight: f64 = new_node.input_weights[i] + (rand::random::<f64>() * 2.0 - 1.0) * lr;
        new_node.input_weights[i] = MIN.max(MAX.min(new_weight));
    }

    let new_fourier_bias: f64 = new_node.fouier_bias + (rand::random::<f64>() * 2.0 - 1.0) * lr;
    new_node.fouier_bias = new_fourier_bias;
    for i in 0..new_node.fourier_n {
        let new_cos: f64 = new_node.cos[i] + (rand::random::<f64>() * 2.0 - 1.0) * lr;
        let new_sin: f64 = new_node.sin[i] + (rand::random::<f64>() * 2.0 - 1.0) * lr;
        new_node.cos[i] = new_cos;
        new_node.sin[i] = new_sin;
    }

    new_node
}

pub fn feedforward(node: &Node, inputs: &[f64]) -> f64 {
    let mut weighted_sum: f64 = 0.0;
    for i in 0..node.num_inputs {
        weighted_sum += node.input_weights[i] * inputs[i];
    }
    weighted_sum /= node.num_inputs as f64;

    let mut output: f64 = node.fouier_bias;
    for i in 0..node.fourier_n {
        output += node.cos[i] * f64::cos(PI * (i as f64 + 1.0) * weighted_sum);
        output += node.sin[i] * f64::sin(PI * (i as f64 + 1.0) * weighted_sum);
    }

    sigmoid(output)
}

pub fn sigmoid(x: f64) -> f64 {
    x / (1.0 + x * x).sqrt()
}

pub fn print(node: &Node) {
    println!("Input Weights:");
    for i in 0..node.num_inputs {
        print!("{}, ", node.input_weights[i]);
    }
    println!("\n");

    println!("Fourier Series:");
    print!("{}+", node.fouier_bias);
    for i in 0..node.fourier_n {
        let val: f64 = PI * (i as f64 + 1.0);
        print!("{}cos({}x)+{}sin({}x)", node.cos[i], val, node.sin[i], val);
    }
    println!();
}
