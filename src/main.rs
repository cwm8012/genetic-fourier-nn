mod node;

fn eval(node: &node::Node) -> f64 {
    let mut error = 0.0;

    let output = node::feedforward(node, &[0.0, 0.0]);
    error += (-1.0 - output).abs();

    let output = node::feedforward(node, &[1.0, 0.0]);
    error += (1.0 - output).abs();

    let output = node::feedforward(node, &[0.0, 1.0]);
    error += (1.0 - output).abs();

    let output = node::feedforward(node, &[1.0, 1.0]);
    error += (-1.0 - output).abs();

    error
}

fn genetic_algorithm() {
    let pop_size: usize = 100;
    let tournament_num: usize = 3;
    let lr: f64 = 0.05;
    let num_inputs: usize = 2;
    let fourier_n: usize = 10;
    let eval_threshold: f64 = 0.1;
    let g_threshold: usize = 10000;

    let mut pop: Vec<node::Node> = Vec::with_capacity(pop_size);
    let mut pop_evals: Vec<f64> = Vec::with_capacity(pop_size);
    let mut best_node: node::Node = node::new(num_inputs, fourier_n);
    let mut best_eval: f64 = eval(&best_node);
    let mut average_eval: f64 = 0.0;

    for _ in 0..pop_size {
        let new_node: node::Node = node::new(num_inputs, fourier_n);
        let new_eval: f64 = eval(&new_node);
        pop.push(new_node.clone());
        pop_evals.push(new_eval);

        average_eval += new_eval;

        if new_eval < best_eval {
            best_node = new_node;
            best_eval = new_eval;
        }
    }
    average_eval /= pop_size as f64;

    let mut g: usize = 0;
    println!("{}: {} {}", g, average_eval, best_eval);
    loop {
        let mut new_pop: Vec<node::Node> = Vec::with_capacity(pop_size);
        let mut new_pop_evals: Vec<f64> = Vec::with_capacity(pop_size);
        let mut new_average_eval: f64 = best_eval;
        new_pop.push(best_node.clone());
        new_pop_evals.push(best_eval);
        for _ in 1..pop_size {
            // selection
            let mut best_selection: usize = rand::random::<usize>() % (pop_size - 1);
            for _ in 0..tournament_num {
                let new_selection: usize = rand::random::<usize>() % (pop_size - 1);
                if pop_evals[new_selection] <= pop_evals[best_selection] {
                    best_selection = new_selection;
                }
            }

            // mutate
            let new_node: node::Node = node::mutate(&pop[best_selection], lr);

            // eval
            let new_eval: f64 = eval(&new_node);
            new_average_eval += new_eval;

            // repopulate
            new_pop.push(new_node.clone());
            new_pop_evals.push(new_eval);

            if new_eval <= best_eval {
                best_node = new_node;
                best_eval = new_eval;
            }
        }
        new_average_eval /= pop_size as f64;

        pop = new_pop;
        pop_evals = new_pop_evals;
        average_eval = new_average_eval;

        g += 1;
        println!("{}: {} {}", g, average_eval, best_eval);
        if best_eval < eval_threshold || g == g_threshold {
            break;
        }
    }

    println!();
    println!("0.0, 0.0 -> {}", node::feedforward(&best_node, &[0.0, 0.0]));
    println!("1.0, 0.0 -> {}", node::feedforward(&best_node, &[1.0, 0.0]));
    println!("0.0, 1.0 -> {}", node::feedforward(&best_node, &[0.0, 1.0]));
    println!("1.0, 1.0 -> {}", node::feedforward(&best_node, &[1.0, 1.0]));

    println!();
    node::print(&best_node);
}

fn main() {
    println!("GA: ");
    println!("Generation: Average Error, Best Error");
    genetic_algorithm();
}
